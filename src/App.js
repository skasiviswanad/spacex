import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      queryParams:{
        launch_year: null,
        launch_success: null,
        land_success: null,
      },
      items: [],
      years:[]
    };
    this.changeHandler = this.changeHandler.bind(this)
  }
  changeHandler = obj =>{
    var queryParams = this.state.queryParams;
    queryParams[obj.key] = obj.value;
    var query = '';
    Object.keys(queryParams).forEach(prop => {
      if(queryParams[prop] != null){
       query += '&'+prop+'='+queryParams[prop]
      }
    })
    fetch("https://api.spaceXdata.com/v3/launches?limit=100&"+query)
    .then(res => res.json())
    .then(
      (result) => {
        this.setState({
          isLoaded: true,
          items: result,
          // queryParams : {[obj.key]:obj.value}
        });
      },
      // Note: it's important to handle errors here
      // instead of a catch() block so that we don't swallow
      // exceptions from actual bugs in components.
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
  }
componentDidMount() {
  fetch("https://api.spaceXdata.com/v3/launches?limit=100")
    .then(res => res.json())
    .then(
      (result) => {
        function filteredYears(res){
          var distinctYears = [];
          res.map(item => {
            if(distinctYears.indexOf(item.launch_year) == -1){
              distinctYears.push(item.launch_year)
            }
          });
          return distinctYears;
        }
        this.setState({
          isLoaded: true,
          items: result,
          years:filteredYears(result)
        });
      },
      // Note: it's important to handle errors here
      // instead of a catch() block so that we don't swallow
      // exceptions from actual bugs in components.
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    )
}
render() {
  const { error, isLoaded, items, years } = this.state;
  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
        <div className="flex-container">
        <div className="filterContentSection">
        <h2>Filters</h2>
          <div className="yearsSection">
          <h3>Launch Year</h3>
          {years.map(year => (
                  <button key={year} className={this.state.queryParams.launch_year == year ? 'triggerButton triggerButtonActive' : 'triggerButton triggerButtonNormal'} onClick={() => this.changeHandler({"param":"launch_year","key":"launch_year","value":year})}> {year} </button>
            ))}
          </div>   
          <div className="launchSection">
            <h3>Successful Launch</h3>
            <button className={this.state.queryParams.launch_success ? 'triggerButton triggerButtonActive' : 'triggerButton triggerButtonNormal'} onClick={() => this.changeHandler({"param":"launch_success","key":"launch_success","value":true})}> True </button>
            <button className={this.state.queryParams.launch_success == false  ? 'triggerButton triggerButtonActive' : 'triggerButton triggerButtonNormal'} onClick={() => this.changeHandler({"param":"launch_success","key":"launch_success","value":false})}> False </button>
          </div>
          <div className="landingSection">
            <h3>Successful Landing</h3>
            <button className={this.state.queryParams.land_success ? 'triggerButton triggerButtonActive' : 'triggerButton triggerButtonNormal'} onClick={() => this.changeHandler({"param":"land_success","key":"land_success","value":true})}> True </button>
            <button className={this.state.queryParams.land_success == false ? 'triggerButton triggerButtonActive' : 'triggerButton triggerButtonNormal'} onClick={() => this.changeHandler({"param":"land_success","key":"land_success","value":false})}> False </button>
          </div>
        </div>
        <div className="mainContentSection">
          {items.map(item => (
            <div className="column" key={item.mission_name+item.flight_number}>
              <div className="card">
                <div className="imageDiv">
                  <img src={item.links.mission_patch_small}  alt={item.links.mission_patch_small} />
                </div>
                <h3>{item.mission_name} #{item.flight_number}</h3>
                <p><b>Mission Ids :</b></p>
                <ul>
                  {item.mission_id.map(id => (
                    <li key={id}>{id}</li>
                  ))}
                </ul>
                <p><b>Launch Year : </b>{item.launch_year}</p>
                <p><b>Successful Launch : </b>{item.launch_success.toString()}</p>
                <p><b>Successful <br/>Landings : </b>{item.mission_name}</p>
              </div>
            </div>
          ))}        
        </div>
        <div>
          Developed By : KASI VISWANATH (skasiviswanad@gmail.com)
        </div>
        </div>
    );
  }
}
}
export default App;